/*global BLOCKS, window, document */
/*jshint loopfunc:true */


(function () {
				
    var game, spec;
    
    spec = {
		width: 1024,
		height: 768,
		bg: {
			src: "images/background.jpg",
			imgArray: ["images/bg01.png", "images/bg02.png", "images/bg03.png", "images/bg04.png", "images/bg05.png", "images/bg06.png"],
			index: 0
		},
		win: {
			src: "images/win.png",
		},
		scoreBars:[{
			name: "scoreBar",
			slices: [{
				name: "1",
				src: "images/bar-01.png"
			}, {
				name: "2",
				src: "images/bar-02.png"
			}, {
				name: "3",
				src: "images/bar-03.png"
			}, {
				name: "4",
				src: "images/bar-04.png"
			}, {
				name: "5",
				src: "images/bar-05.png"
			}, {	
				name: "6",
				src: "images/bar-06.png"
			}]
		}],
		structures:[{
			name: "player",
			slices: [{
				name: "default",
				src: "images/chomp.png",
				numberOfFrames: 2,
				autoPlay: true,
				frameDelay: 50,
				loop: true
			}]
		}],
		glyphs:[{
			name: "glyph01",
			slices: [{
				name: "falling",
				src: "images/block01.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
		}, {
			name: "glyph02",
			slices: [{
				name: "falling",
				src: "images/block02.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
		}, {
			name: "winGlyph",
			slices: [{
				name: "falling",
				src: "images/glyph.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
		}, {
			name: "glyph04",
			slices: [{
				name: "falling",
				src: "images/block04.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
		}, {
			name: "glyph05",
			slices: [{
				name: "falling",
				src: "images/block01.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
		}, {	
			name: "glyph06",
			slices: [{
				name: "falling",
				src: "images/block04.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
			}, {
			name: "glyph07",
			slices: [{
				name: "falling",
				src: "images/block02.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
			}, {
			name: "glyph08",
			slices: [{
				name: "falling",
				src: "images/block04.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
			}, {
			name: "glyph09",
			slices: [{
				name: "falling",
				src: "images/block04.png"
			}, {
				name: "crashed",
				src: "images/block-crash.png"
			}]
		}]
	};
    
	game = BLOCKS.game(spec);
   	
    game.prepare = function () {
   		 
		var bg,
			speed = 13,
			scoreBar, 
			win,
			scoreBars = [],
			index = 0, 
			isTapped = false,
			moveLeft = false,
			moveRight = false,
			colliding = false,
			shouldUpdate = true,
			isDone = false,
			player, 
			structures = [], 
			glyphs = [],
			ground = game.height - 20,
			
			gameTapped = function (point) {
				isTapped = true;
				
				
			},

			gameReleased = function(point) {
				isTapped = false;
				player.removeMotors();
			},

			onKeyUp = function(point) {


			    if (point.keyCode == '37' || point.keyCode == '65') {
			       // left arrow
			       moveLeft = false;
			    }
			    else if (point.keyCode == '39' || point.keyCode == '68') {
			       // right arrow
			       moveRight = false;
			    }

			},

			onKeyDown = function(point) {
			
 
			    if (point.keyCode == '37' || point.keyCode == '65') {
			       // left arrow
			      moveLeft = true;
			    }
			    else if (point.keyCode == '39' || point.keyCode == '68') {
			       // right arrow
			       moveRight = true;
			    }

			},

			updateGame = function(e)
			{
				if (!shouldUpdate) return;

				if (moveLeft == true && player.x > 0-(player.width/2))
					player.x -= 1*speed;
				if (moveRight == true && player.x < game.width+(player.width/2))
					player.x += 1*speed;


				for (i = 0; i < glyphs.length; i += 1) {
					if (glyphs[i].isRectInside(player) && !colliding && glyphs[i].name == "winGlyph") {
						colliding = true;
						destroyGlyph(glyphs[i]);
					
						player.numHits += 1;
						var n = player.numHits + 1;
						
						if (n <= spec.scoreBars[0].slices.length)
						{
							scoreBar.setSlice(n.toString());
						}
						if (n >= spec.scoreBars[0].slices.length)
						{
							victory();
						}
			
					}
				}
						
			},


			//I'LL NEVER DIE!!!!!!
			kill = function (glyph) 
			{
					
						game.addMotor("alpha", {
							object: glyph,
							duration: 300,
							amount: -1,
							easing: "easeIn",
							
						});
						
						glyph.cropHeight = glyph.height;
						game.addMotor("cropHeight", {
							object: glyph,
							duration: 300,
							amount: -glyph.height,
							easing: "easeIn"
						});


						game.addMotor("y", {
							object: glyph,
							duration: 300,
							amount: glyph.height,
							easing: "easeIn"
						});
			},

			//IM GONNA FUCKING LIVE FOREVER!!!!!!!!!!!
			victory = function()
			{
				shouldUpdate = false;
			
				kill(player);

				for (i = 0; i < glyphs.length; i += 1) {
				
					kill(glyphs[i]);
				}

				game.addMotor("alpha", {
							object: scoreBar,
							duration: 500,
							amount: -1,
							easing: "easeIn",
							callback: function () {
								win = BLOCKS.slice(spec.win);
								win.layer = game.layers[3];
								win.width = spec.width;
								win.height = spec.height;
								win.alpha = -1;
								game.stage.addView(win);
								game.addMotor("alpha", {
									object: win,
									duration: 250,
									amount: 1,
									easing: "easeIn",
									callback: function () {
										game.addTicker(loadNewSite, 5000);
									}
								});
							}
						});
			},

			loadNewSite = function()
			{

				 window.location.href = "https://www.devonserket.com";
			},

			cancelCollision = function()
			{
				colliding = false;
			},

			mouseDragged = function (point) {
			
			},
			
			destroyPlayer = function (player) {
			
				var i;
								
				for (i = 0; i < structures.length; i += 1) {
					if (player === structures[i]) {
						player.splice(i, 1);
						break;
					}
				}
				game.stage.removeView(player);
				player.destroy();
				player = null;
			},


			spawnScoreBar = function () {
				
				scoreBar = BLOCKS.block(spec.scoreBars[0]);
				scoreBar.layer = game.layers[3];
				game.stage.addView(scoreBar);
				
				scoreBar.x = game.width - (scoreBar.width * 1.1);
				scoreBar.y = (game.height/2) + (scoreBar.height/2);

				game.addMotor("y", {
					object: scoreBar,
					duration: 500,
					amount: -scoreBar.height,
					easing: "easeOut"
				});
		
				scoreBar.cropHeight = 0;
				game.addMotor("cropHeight", {
					object: scoreBar,
					duration: 500,
					amount: scoreBar.height,
					easing: "easeOut"
				});

				scoreBars.push(scoreBar);
		
			},
			
			
			spawnPlayer = function () {
			
			

			   // Create a block
			   player = BLOCKS.block(spec.structures[0]);

			   player.layer = game.layers[2];
			   
			   player.setSlice(spec.structures[0].slices[0]);
				game.stage.addView(player);
				
				player.x = Math.random() * (game.width - player.width * 2) + player.width / 2;
				player.y = ground;
				player.numHits = 0;
				
				game.addMotor("y", {
					object: player,
					duration: 500,
					amount: -player.height,
					easing: "easeOut"
				});
				
				player.cropHeight = 0;
				game.addMotor("cropHeight", {
					object: player,
					duration: 500,
					amount: player.height,
					easing: "easeOut"
				});
				
				structures.push(player);
			},
			
			destroyGlyph = function (glyph) {
			
				var i;
				glyph.removeMotors();
				
				for (i = 0; i < glyphs.length; i += 1) {
					if (glyph === glyphs[i]) {
						glyphs.splice(i, 1);
						break;
					}
				}
				if (glyph.isRectInside(player)) {	
					game.addTicker(cancelCollision, 500);
				}
				game.stage.removeView(glyph);
				glyph.destroy();
				glyph = null;
			},

			dropGlyph = function () {

				var glyph,
				
					melt = function () {
					
						game.addMotor("alpha", {
							object: glyph,
							duration: 100,
							amount: -1,
							easing: "easeIn",
							callback: function () {
								destroyGlyph(glyph);
							}
						});
						
						glyph.cropHeight = glyph.height;
						game.addMotor("cropHeight", {
							object: glyph,
							duration: 300,
							amount: -glyph.height,
							easing: "easeIn"
						});


						game.addMotor("y", {
							object: glyph,
							duration: 300,
							amount: glyph.height,
							easing: "easeIn"
						});
					};

				glyph = BLOCKS.block(spec.glyphs[Math.floor(Math.random() * spec.glyphs.length)]);
				glyph.layer = game.layers[1];
				var r = Math.random()*1;
				if (r < 0.5)glyph.width *= -1;

				
				game.stage.addView(glyph);
				glyphs.push(glyph);

				glyph.x = Math.random() * (game.width - glyph.width);
				glyph.y = -glyph.height;
				game.addMotor("y", {
					object: glyph,
					duration: (Math.random() * 200) + 5750,
					amount: ground,
					callback: function () {
					
						var i,
						
							resetPlayer = function (player) {
								
							};
						
						
						game.addTicker(melt, 50);

						
					}
				});
				
				game.addTicker(dropGlyph, 350);
			};
		

		bg = BLOCKS.slice(spec.bg);
		bg.layer = game.layers[0];
		bg.width = spec.width;
		bg.height = spec.height;
		game.stage.addView(bg);
		
		
		// game.controller.addEventListener("drag", mouseDragged);
		// game.controller.addEventListener("release", gameReleased);
		game.controller.addEventListener("keyUp", onKeyUp);
		game.controller.addEventListener("keyDown", onKeyDown);
		
		
		spawnPlayer();
		spawnScoreBar();
		var clock = BLOCKS.clock();
		clock.addEventListener('tick', updateGame);
		clock.start();
		dropGlyph();
    };
}());