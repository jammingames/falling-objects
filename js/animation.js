var animation = (function() {
    var delay = 1000 / 30;
    var map = {}, active = [], timer;
    function animate() {
        for(var i=0, l=active.length; i<l; ++i) {
            var data = map[active[i]];
            ++data.index;
            data.index %= data.array.length;
            data.image.src = data.array[data.index];
        }
        timer = setTimeout(animate, delay);
    }
    function begin(e) {
        
        timer = setTimeout(animate, delay);
        
    }
    function end(e) {
        var key = String.fromCharCode(e.keyCode),
            data = map[key];
        if(!data) return;
        data.image.src = data.default;
        var index = active.indexOf(key);
        if(!~index) return;        
        active.splice(index, 1);
        if(!active.length) clearTimeout(timer);
    }
    return {
        add: function(data) {
            data.index = data.index || 0;
            data.image = data.image || data.target.getElementsByTagName('img')[0];
            data.default = data.default || data.image.src;
            map[data.key] = data;
        },
        remove: function(key) {
            delete map[key];
        },
        enable: function() {
        	begin();
           
        },
        disable: function() {
           
            clearTimeout(timer);
            active = [];
        }
    };
})();


animation.add({
    key: 'W',
    target: document.getElementById('jajo'),
    array: [
        "images/bar-01.png",
        "images/bar-02.png",
        "images/bar-03.png"
    ]
});
animation.enable();